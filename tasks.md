Interface
===========
* [ ] Règles temporaires (1 jour) 
* [ ] Historique (3 jours)
* [ ] Lien avec le registre (3 jours)

Gestionnaire de politique
===========
* [ ] Signature de politique (3 jours)

Communication réseau
===========
* [ ] Règles temporaires (3 jours)
* [ ] Lien avec le registre (3 jours)
* [ ] Négotiation (3 jours)