package fr.citi_lab.victor_morel.coiot;

import android.content.Context;

import fr.citi_lab.victor_morel.coiot.R;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewBinderCore;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.binder.AdRecordBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.binder.DeviceInfoBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.binder.HeaderBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.binder.RssiBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.binder.TextBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.AdRecordHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.DeviceInfoHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.HeaderHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.RssiInfoHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.TextHolder;


/*protected*/ final class RecyclerViewCoreFactory {

    public static RecyclerViewBinderCore create(final Context context) {
        final RecyclerViewBinderCore core = new RecyclerViewBinderCore();

        core.add(new TextBinder(context), TextHolder.class, R.layout.list_item_view_textview);
        core.add(new HeaderBinder(context), HeaderHolder.class, R.layout.list_item_view_header);
        core.add(new AdRecordBinder(context), AdRecordHolder.class, R.layout.list_item_view_adrecord);
        core.add(new RssiBinder(context), RssiInfoHolder.class, R.layout.list_item_view_rssi_info);
        core.add(new DeviceInfoBinder(context), DeviceInfoHolder.class, R.layout.list_item_view_device_info);

        return core;
    }

}
