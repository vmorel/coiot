package fr.citi_lab.victor_morel.coiot.details.recyclerview.binder;

import android.content.Context;

import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewItem;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.HeaderHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.model.HeaderItem;


public class HeaderBinder extends BaseViewBinder<HeaderItem> {

    public HeaderBinder(Context context) {
        super(context);
    }

    @Override
    public void bind(BaseViewHolder<HeaderItem> holder, HeaderItem item) {
        final HeaderHolder actualHolder = (HeaderHolder) holder;
        actualHolder.getTextView().setText(item.getText());
    }

    @Override
    public boolean canBind(RecyclerViewItem item) {
        return item instanceof HeaderItem;
    }
}
