package fr.citi_lab.victor_morel.coiot.details.recyclerview.model;

import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewItem;
import uk.co.alt236.bluetoothlelib.device.BluetoothLeDevice;

public class RssiItem implements RecyclerViewItem {

    private final BluetoothLeDevice mDevice;

    public RssiItem(BluetoothLeDevice device) {
        mDevice = device;
    }

    public int getRssi() {
        return mDevice.getRssi();
    }

    public double getRunningAverageRssi() {
        return mDevice.getRunningAverageRssi();
    }

    public int getFirstRssi() {
        return mDevice.getFirstRssi();
    }

    public long getFirstTimestamp() {
        return mDevice.getFirstTimestamp();
    }

    public long getTimestamp() {
        return mDevice.getTimestamp();
    }
}
