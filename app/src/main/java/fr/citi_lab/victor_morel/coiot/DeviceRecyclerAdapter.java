package fr.citi_lab.victor_morel.coiot;


import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseRecyclerViewAdapter;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewBinderCore;

/*package*/ class DeviceRecyclerAdapter extends BaseRecyclerViewAdapter {
    public DeviceRecyclerAdapter(RecyclerViewBinderCore core) {
        super(core);
    }
}
