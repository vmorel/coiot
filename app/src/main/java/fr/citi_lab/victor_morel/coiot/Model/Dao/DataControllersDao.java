package fr.citi_lab.victor_morel.coiot.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.Update;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import fr.citi_lab.victor_morel.coiot.Model.Entity.DataController;

@Dao
public interface DataControllersDao {

    @Insert
    void insert(DataController dc);

    @Delete
    void delete(DataController dc);

    @Update
    void update(DataController dc);

    @Query("Select * from dc_table")
    LiveData<List<DataController>> getAllDataControllers();

}
