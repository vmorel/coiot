package fr.citi_lab.victor_morel.coiot.details.recyclerview.binder;

import android.content.Context;

import fr.citi_lab.victor_morel.coiot.R;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewItem;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.DeviceInfoHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.model.DeviceInfoItem;
import uk.co.alt236.bluetoothlelib.device.BluetoothService;


public class DeviceInfoBinder extends BaseViewBinder<DeviceInfoItem> {

    public DeviceInfoBinder(Context context) {
        super(context);
    }

    @Override
    public void bind(BaseViewHolder<DeviceInfoItem> holder, DeviceInfoItem item) {
        final DeviceInfoHolder actualHolder = (DeviceInfoHolder) holder;

        actualHolder.getName().setText(item.getName());
        actualHolder.getAddress().setText(item.getAddress());
        actualHolder.getDeviceClass().setText(item.getBluetoothDeviceClassName());
        actualHolder.getMajorClass().setText(item.getBluetoothDeviceMajorClassName());
        actualHolder.getBondingState().setText(item.getBluetoothDeviceBondState());
        actualHolder.getServices().setText(createSupportedDevicesString(item));
    }


    private String createSupportedDevicesString(DeviceInfoItem item) {
        final String retVal;

        if (item.getBluetoothDeviceKnownSupportedServices().isEmpty()) {
            retVal = getContext().getString(R.string.no_known_services);
        } else {
            final StringBuilder sb = new StringBuilder();

            for (final BluetoothService service : item.getBluetoothDeviceKnownSupportedServices()) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }

                sb.append(service);
            }
            retVal = sb.toString();
        }

        return retVal;
    }

    @Override
    public boolean canBind(RecyclerViewItem item) {
        return item instanceof DeviceInfoItem;
    }
}
