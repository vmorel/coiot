package fr.citi_lab.victor_morel.coiot.details.recyclerview.binder;

import android.content.Context;

import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewBinder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewItem;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.holder.TextHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.model.TextItem;


public class TextBinder extends BaseViewBinder<TextItem> {

    public TextBinder(Context context) {
        super(context);
    }

    @Override
    public void bind(BaseViewHolder<TextItem> holder, TextItem item) {
        final TextHolder actualHolder = (TextHolder) holder;
        actualHolder.getTextView().setText(item.getText());
    }

    @Override
    public boolean canBind(RecyclerViewItem item) {
        return item instanceof TextItem;
    }
}
