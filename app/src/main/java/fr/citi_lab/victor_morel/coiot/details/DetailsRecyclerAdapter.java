package fr.citi_lab.victor_morel.coiot.details;

import java.util.List;

import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseRecyclerViewAdapter;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewBinderCore;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewItem;


/*package*/ class DetailsRecyclerAdapter extends BaseRecyclerViewAdapter {
    public DetailsRecyclerAdapter(RecyclerViewBinderCore core, List<RecyclerViewItem> items) {
        super(core, items);
    }

//    private static List<RecyclerViewItem> validate(RecyclerViewBinderCore core, List<RecyclerViewItem> items) {
//        final List<RecyclerViewItem> retVal = new ArrayList<>();
//
//        for (final RecyclerViewItem item : items) {
//            if (core.getViewType(item) != RecyclerViewBinderCore.INVALID_VIEWTYPE) {
//                retVal.add(item);
//            }
//        }
//
//        return retVal;
//    }
}
