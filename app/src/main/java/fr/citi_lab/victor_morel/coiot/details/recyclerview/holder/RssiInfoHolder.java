package fr.citi_lab.victor_morel.coiot.details.recyclerview.holder;

import android.view.View;
import android.widget.TextView;

import fr.citi_lab.victor_morel.coiot.R;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.model.RssiItem;


public class RssiInfoHolder extends BaseViewHolder<RssiItem> {

    private final TextView mTvFirstTimestamp;
    private final TextView mTvFirstRssi;
    private final TextView mTvLastTimestamp;
    private final TextView mTvLastRssi;
    private final TextView mTvRunningAverageRssi;

    public RssiInfoHolder(View itemView) {
        super(itemView);

        mTvFirstTimestamp = (TextView) itemView.findViewById(R.id.firstTimestamp);
        mTvFirstRssi = (TextView) itemView.findViewById(R.id.firstRssi);
        mTvLastTimestamp = (TextView) itemView.findViewById(R.id.lastTimestamp);
        mTvLastRssi = (TextView) itemView.findViewById(R.id.lastRssi);
        mTvRunningAverageRssi = (TextView) itemView.findViewById(R.id.runningAverageRssi);
    }

    public TextView getFirstTimestamp() {
        return mTvFirstTimestamp;
    }

    public TextView getFirstRssi() {
        return mTvFirstRssi;
    }

    public TextView getLastTimestamp() {
        return mTvLastTimestamp;
    }

    public TextView getLastRssi() {
        return mTvLastRssi;
    }

    public TextView getRunningAverageRssi() {
        return mTvRunningAverageRssi;
    }
}
