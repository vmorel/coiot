package fr.citi_lab.victor_morel.coiot.details.recyclerview.holder;

import android.view.View;
import android.widget.TextView;

import fr.citi_lab.victor_morel.coiot.R;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.BaseViewHolder;
import fr.citi_lab.victor_morel.coiot.details.recyclerview.model.HeaderItem;


public class HeaderHolder extends BaseViewHolder<HeaderItem> {

    private final TextView mText;

    public HeaderHolder(View itemView) {
        super(itemView);

        mText = (TextView) itemView.findViewById(R.id.text);
    }

    public TextView getTextView() {
        return mText;
    }
}
