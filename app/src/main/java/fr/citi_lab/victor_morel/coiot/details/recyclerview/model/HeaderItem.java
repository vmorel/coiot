package fr.citi_lab.victor_morel.coiot.details.recyclerview.model;


import fr.citi_lab.victor_morel.coiot.details.recyclerview.RecyclerViewItem;

public class HeaderItem implements RecyclerViewItem {
    private final CharSequence mText;

    public HeaderItem(CharSequence text) {
        mText = text;
    }

    public CharSequence getText() {
        return mText;
    }
}
